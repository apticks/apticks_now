<?php

class Subscribe_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="subscribe_details";
            $this->primary_key="id";
           
            $this->config();
            $this->forms();
            $this->relations();
    }
    // protected function _add_created_by($data)
    // {
    //     $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
    //     return $data;
    // }
    // protected function _add_updated_by($data)
    // {
    //     $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
    //     return $data;
    // } 
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'first_name',
                'label' => 'Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 5 characters'
                )
            )
        );
    }
    function subsribe_check($data)
    {
            $this->db->select('*');
            $this->db->from('subscribe_details');
            $this->db->where('email',$data);           
            //$this->db->where('CD_GROUP_ID',3);
            $query=$this->db->get();
            return $query->result();
    }
}

