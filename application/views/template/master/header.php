<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="<?php echo site_url();?>" class="logo">
                    <img src="<?php echo site_url('master_assets/img/apticks_logo.svg')?>"   width="230px" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="<?php echo site_url();?>" class="logo">
                    <img src="<?php echo site_url('master_assets/img/apticks_logo.svg')?>"  width="230px"alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li class="">
                    <a href="<?php echo site_url();?>">Home</a>
                </li>
                <li class="menu-item-has-children">
                    <a href="<?php echo site_url('about_us');?>">Company</a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo site_url('about_us');?>">About us</a></li>
                        <!-- <li><a href="<?php //echo site_url('our_team');?>">Our team</a></li> -->
                        <li><a href="<?php echo site_url('work_process');?>">Work process</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="<?php echo site_url('services');?>">Services</a>
                    <ul class="sub-menu">
                   		<li><a href="<?php echo site_url('software_developement');?>">Software Development</a></li>
                        <li><a href="<?php echo site_url('web_developement');?>">Web Development</a></li>
                        <li><a href="<?php echo site_url('mobile_app_developement');?>">Mobile App Development</a></li>
                        <li><a href="<?php echo site_url('branding_and_identity');?>">Branding & Identity</a></li>
                        <li><a href="<?php echo site_url('digital_marketing');?>">Digital Marketing</a></li>
                    </ul>
                </li>
                <!-- <li class="menu-item-has-children">
                    <a href="<?php echo site_url('all_internship_programs');?>">Internships</a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo site_url('full_stack');?>">Full Stack Program</a></li>
                        <li><a href="<?php echo site_url('android');?>">Android</a></li>
                        <li><a href="<?php echo site_url('ui_ux');?>">UI/UX Design</a></li>
                    </ul>
                </li> -->
                <!--<li class="">
                    <a href="<?php echo site_url('our_portfolio');?>">Portfolio</a>
                </li> -->
                <!--<li class="">
                    <a href="<?php echo site_url('job_posts');?>">Career</a>
                </li> -->
                <li>
                    <a href="<?php echo site_url('contact_us');?>">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- navbar area end -->