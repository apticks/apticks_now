<!-- jquery -->
<script src="<?php echo base_url()?>master_assets/js/jquery-2.2.4.min.js"></script>
<!-- popper -->
<script src="<?php echo base_url()?>master_assets/js/popper.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo base_url()?>master_assets/js/bootstrap.min.js"></script>
<!-- magnific popup -->
<script src="<?php echo base_url()?>master_assets/js/jquery.magnific-popup.js"></script>
<!-- wow -->
<script src="<?php echo base_url()?>master_assets/js/wow.min.js"></script>
<!-- owl carousel -->
<script src="<?php echo base_url()?>master_assets/js/owl.carousel.min.js"></script>
<!-- slick slider -->
<script src="<?php echo base_url()?>master_assets/js/slick.js"></script>
<!-- cssslider slider -->
<script src="<?php echo base_url()?>master_assets/js/jquery.cssslider.min.js"></script>
<!-- waypoint -->
<script src="<?php echo base_url()?>master_assets/js/waypoints.min.js"></script>
<!-- counterup -->
<script src="<?php echo base_url()?>master_assets/js/jquery.counterup.min.js"></script>
<!-- imageloaded -->
<script src="<?php echo base_url()?>master_assets/js/imagesloaded.pkgd.min.js"></script>
<!-- isotope -->
<script src="<?php echo base_url()?>master_assets/js/isotope.pkgd.min.js"></script>
<!-- world map -->
<script src="<?php echo base_url()?>master_assets/js/worldmap-libs.js"></script>
<script src="<?php echo base_url()?>master_assets/js/worldmap-topojson.js"></script>
<script src="<?php echo base_url()?>master_assets/js/mediaelement.min.js"></script>
<!-- main js -->
<script src="<?php echo base_url()?>master_assets/js/main.js"></script>
<!-- google map js -->
<script src="<?php echo base_url()?>master_assets/js/goolg-map-activate.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVyNXoXHkqAwBKJaouZWhHPCP5vg7N0HQ&amp;callback=initMap"></script>
<script src="<?php echo base_url()?>master_assets/js/mediaelement.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
 -->
<script type="text/javascript">

// For Contact Us Page By Trupti
$(document).ready(function() 
{
  $('#contact_form').submit(function(){
   
    $('#contact_data').html("<b>Loading response...</b>");
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url('home/contact') ?>',
            data: $(this).serialize()
        })
        .done(function(data){
           $('#contact_data').html(data);
        })
        .fail(function() {
         alert( "Something Went Wrong! Please Try Again!" );
            
        });
      return false;
    });
});
</script>

<script>
$(document).ready(function(){
    $('#subscriber_form').submit(function(){
    $('#response').html("<b>Loading response...</b>");
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url('home/subscribe_submit') ?>',
            data: $(this).serialize()
        })
        .done(function(data){
           $('#response').html(data);
        })
        .fail(function() {
         alert( "Something Went Wrong! Please Try Again!" );
            
        });
      return false;
    });

});
</script>