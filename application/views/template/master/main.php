<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Apticks-<?php echo $title; ?></title>
    <!-- favicon -->
    <link rel=icon href="<?php echo base_url()?>master_assets/img/favicon.ico">
	<?php $this->load->view('template/master/topcss');?>
</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->
<?php $this->load->view('template/master/header');?>

<?php $this->load->view($content);?>

<?php $this->load->view('template/master/footer');?>

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->

<?php $this->load->view('template/master/scripts');?>
</body>
</html>