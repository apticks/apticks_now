<!-- animate -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/animate.css">
<!-- bootstrap -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/bootstrap.min.css">
<!-- magnific popup -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/magnific-popup.css">
<!-- owl carousel -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/owl.carousel.min.css">
<!-- icons -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/line-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/flaticon.css">
<!-- slick slider -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/slick.css">
<!-- animated slider -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/animated-slider.css">
<!-- Main Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/style.css">
<!-- Custom Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/custom.css">
<!-- responsive Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url()?>master_assets/css/responsive.css">
<!--Codingeek -->
<!-- <link rel="stylesheet" href="../../codingeek-js/codingeek.css">
 -->