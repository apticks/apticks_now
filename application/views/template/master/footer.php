<!-- <?php if ($this->session->flashdata('sub_success')) { ?>
<div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>Success!</strong>
    <?php echo $this->session->flashdata('sub_success'); ?>
</div>
<?php } ?> -->
<footer class="footer-area footer-area-2 pd-top-105">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-11 text-center">
                <div class="section-title text-center">
                    <h2 class="title">Get our latest <span>Newslatter</span></h2>
                    <p>Offer your business with the best assistance for growth.</p>
                </div>
                <div class="newsletter-subcribe"><div id='response'></div>
                    <form class="subcribe-form" enctype="multipart/form-data" name="subscriber_form" id="subscriber_form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your E-mail..." name="email" required=""  id="user_subs_email">
                            <button type="submit" class="btn-blue subcribe-submit" id="user_subcribe_submit">submit</button>
                        </div>
                    </form>
           <!--    <div class="alert alert-success alert-dismissible"><div id='response'></div>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Success!</strong> This alert box could indicate a successful or positive action.
  </div>  -->
                </div>
            </div>
        </div>
        <div class="footer-widget-area mg-top-120">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget">
                        <div class="about_us_widget">
                            <a href="<?php echo site_url();?>" class="footer-logo"> 
                                <img src="<?php echo site_url('master_assets/img/apticks_logo.svg');?>" width="230px"  alt="footer logo">
                            </a>
                            <p>Apticks believes that the perfect leadership alone will bring the revolutions, Not simply the Technology. Each skilled at Apticks has unique leadership qualities, Who area unit determined, fearless, and skilful. Their spirit and values area unit contagious that inspire the complete team to deliver their best to our reputable clients.</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="linkedin" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget ">
                        <h4 class="widget-title">Contact</h4>
                        <div class="contact_info_list">
                            <p class="contact-content">Madhapur, Hyderabad.</p>
                            <p><span>Contact:</span> +91 7013907291</p>
                            <p><span>E-mail:</span> info@apticks.com, apticksit@gmail.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget widget_nav_menu">
                        <h4 class="widget-title">Services</h4>
                        <ul>
                        	<li><a href="<?php echo site_url('software_developement');?>">Software Development</a></li>
                            <li><a href="<?php echo site_url('web_developement');?>">Web Development</a></li>
                            <li><a href="<?php echo site_url('mobile_app_developement');?>">Mobile App Development</a></li>
                            <li><a href="<?php echo site_url('branding_and_identity');?>">Branding & Identity</a></li>
                            <li><a href="<?php echo site_url('digital_marketing');?>">Digital Marketing</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget widget_nav_menu">
                        <h4 class="widget-title">Quick Link</h4>
                        <ul>
                            <li><a href="<?php echo site_url();?>">Home</a></li>
                            <li><a href="<?php echo site_url('about_us');?>">About Us</a></li>
                            <li><a href="<?php echo site_url('work_process');?>">Work process</a></li>
                            <!-- <li><a href="<?php echo site_url('our_portfolio');?>">Portfolio</a></li>
                            <li><a href="<?php echo site_url('job_posts');?>">Career</a></li> -->
                            <li><a href="<?php echo site_url('contact_us');?>">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                
                <!--<div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget widget_nav_menu">
                        <h4 class="widget-title">Internships</h4>
                        <ul>
                        	<li><a href="<?php echo site_url('full_stack');?>">Full Stack Program</a></li>
                            <li><a href="<?php echo site_url('android');?>">Android</a></li>
                            <li><a href="<?php echo site_url('ui_ux');?>">UI/UX Design</a></li>
                        </ul>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <div class="copyright-inner">
        <div class="copyright-text">
            &#169; Apticks 2019 All rights reserved.
        </div>
    </div>
</footer>