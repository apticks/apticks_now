<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="index.html"> <img alt="image" src="<?php echo base_url()?>assets/img/logo.png" class="header-logo" /> 
                            <!--<span class="logo-name">Aegis</span>-->
						</a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture">
							<img alt="image" src="<?php echo base_url()?>assets/img/userbig.png">
						</div>
						<div class="sidebar-user-details">
							<div class="user-name"><?php echo $user->email;?></div>
							<div class="user-role"><?php echo $user->first_name.''.$user->last_name;?></div>
						</div>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active"><a href="<?php echo base_url('dashboard');?>" class="nav-link "><i
									data-feather="monitor"></i><span>Dashboard</span>
							</a>
							
						</li>
                        <li class="dropdown active"><a href="<?php echo base_url('contacts');?>" class="nav-link "><i
                                    data-feather="monitor"></i><span>Our Contacts</span>
                            </a>
                            
                        </li>
                          <li class="dropdown active"><a href="<?php echo base_url('admin/portfolio/r');?>" class="nav-link "><i
                                    data-feather="monitor"></i><span>Porfolio</span>
                            </a>
                            
                        </li>
    					
                     
					</ul>
				</aside>
			</div>