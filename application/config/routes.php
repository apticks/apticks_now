<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/*
 *Admin 
 */
$route['dashboard'] = 'admin/dashboard';
$route['settings/(:any)'] = 'admin/settings/$1';


/**
 * Home Controler routes
 */
/*----Company---*/
$route['about_us'] = 'home/company/about_us';
$route['contact_us'] = 'home/company/contact_us';
$route['our_team'] = 'home/company/our_team';
$route['work_process'] = 'home/company/work_process';
$route['contact'] = 'home/contact';
/*----Services---*/
$route['services'] = 'home/services/services';
$route['software_developement'] = 'home/services/software_developement';
$route['web_developement'] = 'home/services/web_developement';
$route['mobile_app_developement'] = 'home/services/mobile_app_developement';
$route['branding_and_identity'] = 'home/services/branding_and_identity';
$route['business_consultancy'] = 'home/services/business_consultancy';
$route['ui_ux_design'] = 'home/services/ui_ux_design';
$route['digital_marketing'] = 'home/services/digital_marketing';


/*----Internship---*/
$route['all_internship_programs'] = 'home/internship_program/all_internship_programs';
$route['android'] = 'home/internship_program/android';
$route['ios'] = 'home/internship_program/ios';
$route['ui_ux'] = 'home/internship_program/ui_ux';
$route['full_stack'] = 'home/internship_program/full_stack';

/*----Protfolio---*/
$route['our_portfolio'] = 'home/portfolio/our_portfolio';
$route['dialmana'] = 'home/portfolio/dialmana';
$route['limpex'] = 'home/portfolio/limpex';
$route['tradebea'] = 'home/portfolio/tradebea';
$route['nextclick'] = 'home/portfolio/nextclick';

/*----Career---*/
$route['job_posts'] = 'home/careers/job_posts';
$route['job_desc'] = 'home/careers/job_desc';

/*----Common---*/
$route['coming_soon'] = 'home/common/coming_soon';
$route['privacy_policy'] = 'home/common/privacy_policy';
$route['faq'] = 'home/common/faq';
