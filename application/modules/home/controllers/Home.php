<?php

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/master/main';
         
        $this->load->model('contact_model');
        $this->load->model('portfolio_details_model');
        $this->load->helper('common_helper');
        $this->load->helper('email_helper');
        $this->load->library('email');
        $this->load->model('subscribe_model');
        $this->load->model('user_model');
    }
    
    public function index(){
        $this->data['title'] = 'home';
        $this->data['nav'] = 'home';
        $this->data['content'] = 'home/home';
        $this->_render_page($this->template, $this->data);
    }
    
    public function company($type = 'about_us'){
        if($type == 'about_us'){
            $this->data['title'] = 'About';
            $this->data['nav'] = 'about';
            $this->data['content'] = 'home/company/about_us';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'our_team'){
            $this->data['title'] = 'Our Team';
            $this->data['nav'] = 'our_team';
            $this->data['content'] = 'home/company/our_team';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'work_process'){
            $this->data['title'] = 'Work Process';
            $this->data['nav'] = 'work_process';
            $this->data['content'] = 'home/company/work_process';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'contact_us'){
            $this->data['title'] = 'Contact Us';
            $this->data['nav'] = 'contact_us';
            $this->data['content'] = 'home/company/contact_us';
            $this->_render_page($this->template, $this->data);
        }
        
    }
    
    public function services($type = "services"){
        if($type == 'services'){
            $this->data['title'] = 'Services';
            $this->data['nav'] = 'services';
            $this->data['content'] = 'home/services/all_services';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'software_developement'){
            $this->data['title'] = 'Software Developement';
            $this->data['nav'] = 'software_developement';
            $this->data['content'] = 'home/services/software_developement';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'web_developement'){
            $this->data['title'] = 'Web Development';
            $this->data['nav'] = 'web_developement';
            $this->data['content'] = 'home/services/web_developement';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'mobile_app_developement'){
            $this->data['title'] = 'Mobile App Development';
            $this->data['nav'] = 'mobile_app_developement';
            $this->data['content'] = 'home/services/mobile_app_developement';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'branding_and_identity'){
            $this->data['title'] = 'Branding And Identity';
            $this->data['nav'] = 'branding_and_identity';
            $this->data['content'] = 'home/services/branding_and_identity';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'business_consultancy'){
            $this->data['title'] = 'Business Consultancy';
            $this->data['nav'] = 'business_consultancy';
            $this->data['content'] = 'home/services/business_consultancy';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'ui_ux_design'){
            $this->data['title'] = 'UI UX Design';
            $this->data['nav'] = 'ui_ux_design';
            $this->data['content'] = 'home/services/ui_ux_design';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'digital_marketing'){
            $this->data['title'] = 'Digital Marketing';
            $this->data['nav'] = 'digital_marketing';
            $this->data['content'] = 'home/services/digital_marketing';
            $this->_render_page($this->template, $this->data);
        }
    }
    
    public function internship_program($type = 'all_internship_programs'){
        if($type == 'all_internship_programs'){
            $this->data['title'] = 'All Internship Programs';
            $this->data['nav'] = 'all_internship_programs';
            $this->data['content'] = 'home/internship/all_internship_programs';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'android'){
            $this->data['title'] = 'Android';
            $this->data['nav'] = 'android';
            $this->data['content'] = 'home/internship/android';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'ios'){
            $this->data['title'] = 'IOS';
            $this->data['nav'] = 'ios';
            $this->data['content'] = 'home/internship/ios';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'ui_ux'){
            $this->data['title'] = 'UI UX';
            $this->data['nav'] = 'ui_ux';
            $this->data['content'] = 'home/internship/ui_ux';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'full_stack'){
            $this->data['title'] = 'Full Stack';
            $this->data['nav'] = 'full_stack';
            $this->data['content'] = 'home/internship/full_stack';
            $this->_render_page($this->template, $this->data);
        }
    }
    
    public function careers($type = 'job_posts'){
        if($type == 'job_posts'){
            $this->data['title'] = 'Job Postings';
            $this->data['nav'] = 'job_posts';
            $this->data['content'] = 'home/career/job_posts';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'job_desc'){
            $this->data['title'] = 'Job Description';
            $this->data['nav'] = 'job_desc';
            $this->data['content'] = 'home/career/job_desc';
            $this->_render_page($this->template, $this->data);
        }
    }
    
    public function portfolio($type = 'our_portfolio'){
        if($type == 'our_portfolio'){
            $this->data['title'] = 'Our Protfolio';
            $this->data['nav'] = 'our_portfolio';
            $this->data['content'] = 'home/portfolio/our_portfolio';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'dialmana'){
            $this->data['title'] = 'Dialmama';
            $this->data['nav'] = 'dialmana';
            $this->data['content'] = 'home/portfolio/dialmana';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'limpex'){
            $this->data['title'] = 'Limpex';
            $this->data['nav'] = 'limpex';
            $this->data['content'] = 'home/portfolio/limpex';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'tradebea'){
            $this->data['title'] = 'Tradebea';
            $this->data['nav'] = 'tradebea';
            $this->data['content'] = 'home/portfolio/tradebea';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'nextclick'){
            $this->data['title'] = 'Nextclick';
            $this->data['nav'] = 'nextclick';
            $this->data['content'] = 'home/portfolio/nextclick';
            $this->_render_page($this->template, $this->data);
        }
    }
    
    public function common($type = 'coming_soon'){
        if($type == 'coming_soon'){
            $this->data['title'] = 'Coming Soon';
            $this->data['nav'] = 'coming_soon';
            $this->data['content'] = 'home/coming_soon';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'privacy_policy'){
            $this->data['title'] = 'Privacy Policy';
            $this->data['nav'] = 'privacy_policy';
            $this->data['content'] = 'home/privacy_policy';
            $this->_render_page($this->template, $this->data);
        }elseif ($type == 'faq'){
            $this->data['title'] = 'FAQ';
            $this->data['nav'] = 'faq';
            $this->data['content'] = 'home/faq';
            $this->_render_page($this->template, $this->data);
        }
    }
     
   
    public function contact()
   {
      
        $data = $this->input->post('email');
       
          
         $result=$this->contact_model->contact_check($data);
         if(count(array_filter($result)))
        {
              echo "<p style='color:red';>Sorry!! Email Already exist.!</p>";
        }else
        {
           $data = array(
                'name' => $this->input->post('name'),
                'email'=>$this->input->post('email'),
                'subject' =>$this->input->post('subject'),
                'message' => $this->input->post('message'),
                'mobile' => $this->input->post('mobile')
            );
            $user_id=$this->contact_model->insert($data);
            echo "<p style='color:green';>Thank You! We will get back in touch with you soon! </p>";   
               //    $send = sendEmail($this->input->post('email'), $to = 'tptrupti011@gmail.com', $this->input->post('subject'), $this->input->post('message'), null, null, null, null);
               // print_r($send);
               $send =  mail('tptrupti011@gmail.com', $this->input->post('subject'), $this->input->post('message'));
               //print_r($send);
           }
    }
    public function subscribe_submit()
    {
         $data = $this->input->post('email');
       
         $result=$this->subscribe_model->subsribe_check($data);        
                if(count(array_filter($result)))
        {
              echo "<p style='color:red';>Sorry!! Email Already exist.!</p>";
        }else
        {
            $data= array(
                       'email'=>$data
                    );
            $user_id=$this->subscribe_model->insert($data);
            echo "<p style='color:green';>Thank you, You have successfully subscribed</p>";   
              // if (! empty($user_id))
              //       $this->session->set_flashdata('sub_success', 'Subscribed Successfully..!');
              //   else
              //       $this->session->set_flashdata('error', 'Something went wrong..!');
        }
       
}
}

