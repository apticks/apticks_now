<!-- header area start -->
<div class="header-area header-bg" style="background-image:url(<?php echo base_url()?>master_assets/img/bg/banner-bg.png);">
    <div class="container">
        <div class="banner-slider banner-slider-one">
            <div class="banner-slider-item">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-6 offset-xl-1">
                        <div class="header-inner-details">
                            <div class="header-inner">
                                <h1 class="title s-animate-1">Turning All Ideas Into Real <span>Success</span></h1>
                                <p class="s-animate-2"></p>
                                <!-- <div class="btn-wrapper desktop-left padding-top-20">
                                    <a href="#" class="btn btn-radius btn-green s-animate-3">Read More</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 hidden-sm">
                        <div class="banner-thumb-wrap">
                            <div class="banner-thumb">
                                <img class="header-inner-img" src="<?php echo base_url()?>master_assets/img/banner/1.png" alt="banner-img">
                                <img class="banner-1-img2" src="<?php echo base_url()?>master_assets/img/banner/2.png" alt="banner-img">
                                <img class="banner-1-img3" src="<?php echo base_url()?>master_assets/img/banner/3.png" alt="banner-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-slider-item">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-6  offset-xl-1">
                        <div class="header-inner-details">
                            <div class="header-inner">
                                <h1 class="title s-animate-1">Crafting Online <span>Software</span> For You</h1>
                                <p class="s-animate-2"></p>
                                <!-- <div class="btn-wrapper desktop-left padding-top-20">
                                    <a href="#" class="btn btn-radius btn-green s-animate-3">Read More</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 hidden-sm">
                        <div class="banner-thumb-wrap">
                            <div class="banner-thumb">
                                <img class="header-inner-img" src="<?php echo base_url()?>master_assets/img/banner/01.png" alt="banner-img">
                                <img class="banner-1-img2" src="<?php echo base_url()?>master_assets/img/banner/02.png" alt="banner-img">
                                <img class="banner-1-img3" src="<?php echo base_url()?>master_assets/img/banner/03.png" alt="banner-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header area End -->

<!-- service area start -->
<div class="service-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center margin-bottom-90">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">What <span>Service</span> We are Providing?</h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Offer your business with the best assistance for growth.</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-16">
            <div class="col-xl-4 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.1s">
                    <img src="<?php echo base_url()?>master_assets/img/service/software-development.svg" alt="service">
                    <h6><a href="<?php echo site_url('software_developement');?>">Software Development</a></h6>
                    <p>Apticks software development services allow you to process high-quality, demanding jobs of web marketing, application development, customization, and social media integration services.</p>
                    <div class="read-more">
                        <a href="<?php echo site_url('software_developement');?>"><img src="<?php echo base_url()?>master_assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">
                    <img src="<?php echo base_url()?>master_assets/img/service/web-app-development.svg" alt="service">
                    <h6><a href="<?php echo site_url('web_developement');?>">Web Design & Development</a></h6>
                    <p>We are experienced professionals web design development services from many years, Who can understand and determine what the company or its customers&rsquo; needs within the present IT industry.</p>
                    <div class="read-more">
                        <a href="<?php echo site_url('web_developement');?>"><img src="<?php echo base_url()?>master_assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                    <img src="<?php echo base_url()?>master_assets/img/service/mobile-app-development.svg" alt="service">
                    <h6><a href="<?php echo site_url('mobile_app_developement');?>">Mobile App Development</a></h6>
                    <p>At Apticks we offer a wide range of mobile application development services, And build customized apps which will meet the requirements of any individual client.</p>
                    <div class="read-more">
                        <a href="<?php echo site_url('mobile_app_developement');?>"><img src="<?php echo base_url()?>master_assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <img src="<?php echo base_url()?>master_assets/img/service/branding.svg" alt="service">
                    <h6><a href="<?php echo site_url('branding_and_identity');?>">Branding Identity</a></h6>
                    <p>With our effective Graphic designing services, we create unique corporate identity and recognizable logos, images, and photos through our creation & restoration services. </p>
                    <div class="read-more">
                        <a href="<?php echo site_url('branding_and_identity');?>"><img src="<?php echo base_url()?>master_assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div> 
            <div class="col-xl-4 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <img src="<?php echo base_url()?>master_assets/img/service/digital-marketing.svg" alt="service">
                    <h6><a href="<?php echo site_url('digital_marketing');?>">Digital Marketing</a></h6>
                    <p>We believe investing mindful thinking while promoting our customers&rsquo; businesses and with this intention we help our customers to create a positive reputation.</p>
                    <div class="read-more">
                        <a href="<?php echo site_url('digital_marketing');?>"><img src="<?php echo base_url()?>master_assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
<!-- service area End -->

<!-- soft-box area start -->
<div class="sbs-what-riyaqas pd-default-120 mg-top-105" style="background-image:url(<?php echo base_url()?>master_assets/img/bg/1h1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <img src="<?php echo base_url()?>master_assets/img/business-tool/1.png" alt="img">
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                        <h2 class="title">What is <span>Apticks?</span></h2>
                        <p>Apticks is at the forefront as the most innovative web development company. Our cost efficient and structured teamwork justifies our position. Apticks was setup to leverage the "Proven India Advantage" in delivering low cost IT software services with a guarantee of Quality with Speed. We have evolved into a large team of professionals with diverse skills and experience.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url()?>master_assets/img/icons/check.svg" alt="check">
                                <span>Concept Design</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url()?>master_assets/img/icons/check.svg" alt="check">
                                <span>Concept Implementation</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url()?>master_assets/img/icons/check.svg" alt="check">
                                <span>Concept Development</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url()?>master_assets/img/icons/check.svg" alt="check">
                                <span>Testing work</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- soft-box area End -->

<!-- client area start -->
<div class="client-area mg-top-100 bg-gray pd-top-110 pd-bottom-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9">
                <div class="section-title style-two text-md-center">
                    <h2 class="title">Our valuable <span>Client</span></h2>
                    <p>Apticks One of the best Software Development company. Apticks is at the forefront as the most <br> innovative web development company. Our cost efficient and structured teamwork.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="client-slider-2">
                    <div class="item"  style="margin: 0 2rem;" >
                        <a href="#" class="thumb">
                            <img src="<?php echo base_url()?>master_assets/img/client/logo_dialmama.jpg" width="167x" height="100px" alt="client">
                        </a>
                    </div>
                    <div class="item"  style="margin: 0 2rem;" >
                        <a href="#" class="thumb">
                            <img src="<?php echo base_url()?>master_assets/img/client/limpex_logo.png" width="167x" height="100px" alt="client">
                        </a>
                    </div>
                    <div class="item"  style="margin: 0 2rem;" >
                        <a href="#" class="thumb">
                            <img src="<?php echo base_url()?>master_assets/img/client/next_logo.svg"  width="167x" height="100px" alt="client">
                        </a>
                    </div>
                    <div class="item"  style="margin: 0 2rem;" >
                        <a href="#" class="thumb">
                            <img src="<?php echo base_url()?>master_assets/img/client/tradebea_logo.svg" width="167x" height="100px" alt="client">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- client area End -->

