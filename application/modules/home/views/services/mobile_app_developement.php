 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>Mobile Application Development</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>master_assets/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>master_assets/img/service/mobile_app_development/android.svg" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title"><span>Android</span> Application Development</h2>
                        <p>At Apticks we offer a wide range of android application development services, and build customized apps which will meet the requirements of any individual client. we've a strong and experienced team of developers and engineers which will tackle any challenge and work on any Android platform.</p>
                        <p>With us, you'll be able to convert your ideas into functional, user-friendly and easy-to-use applications, which significantly increase your chances of success at the worldwide Android market, and supply you with many various options for further development and advance.</p>
                        <p>Our team has vast experience in creating every kind of Android applications, including native Android applications, web-based Android-compatible applications, or hybrid applications.</p>
                        <p>Android with 85% market share in mobiles, must be the primary choice for any Mobile App development.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<!-- IOS area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about"><span>IOS</span> Application Development</h2>
                        <p>We offer applications including business and sales applications, games development, widget development, web service integration, Mobile Website Development, social networking, e-commerce solutions, traveling navigation, and weather forecasting applications.</p>
                        <p>Apticks contains a strong team in iPhone and iPad application development team. We specialise in developing custom apps for our clients from scratch, we can also customize or maintain an already developed app. We develop apps from simple applications to complex applications.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/mobile_app_development/apple.svg" alt="video">
                </div>
            </div>
            
        </div>
    </div>

<!-- IOS area End -->
<br>
<br>
<!-- Hybrid area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/mobile_app_development/flutter.svg" alt="video">
                </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about"><span>Hybrid</span> Application Development</h2>
                        <p>The graph of mobile user is increasing day by day. Currently they're billions of mobile devices running different operating systems and platforms like Android, iOS, Windows and Blackberry. The main problem faced by many developers is certainly trying to develop apps for all of them. Hybrid apps allow developer to leverage all the favored mobile platforms and it's an ideal character combination of native and web-based applications.</p>
                        <p>Our teams have significant experience hybrid app development and that we provide our client hybrid app which will highlight on all app stores and increases business attention within the mobile industry. </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<!-- Hybrid area End -->
