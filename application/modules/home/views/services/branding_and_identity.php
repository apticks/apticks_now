 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>Branding Identity Service</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>master_assets/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>master_assets/img/service/branding_identity/identity.svg" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title">What is <span>Branding identity?</span></h2>
                        <p>Branding identity Services is a part of web development service. this is an important job that enriches the overall aesthetic experience of a web site. In essence, Graphic Designing development process circumambulates several different phases of designing, smoothing, resizing, and submitting to our dedicated web development workgroup to feature, to schedule, and to launch for a newly created website.</p>
                        <h6 class="sub-title">Services on which we focus:</h6>
                        <ul>
                        	<li>Corporate Branding / Unique Identity Design</li>
                        	<li>Unique Logo Design, Ads Design, & Themes Design</li>
                        	<li>Web Hosting, Domain Registration</li>
                        	<li>Website Design</li>
                        	<li>Search Engine Optimization, Flash Animation</li>
                        </ul>
                        <p>With our effective Graphic designing services, we create unique corporate identity and recognizable logos, images, and photos through our creation & restoration services. We possess a team of highly qualified & certified web designers, who are very experienced, proficient, and competent solution providers in this industry.</p>
                        <p>With our marketing place positions & online business theme, we boost our client&rsquo;s expectations with high end business solutions, which improve their revenue and profitability, and make them, stay ahead of the competition. We deliver various domains of visual effects for Graphic designing services process including the brand recognition, brand logo, product packaging, brochures and literatures.</p>
                        <p>We have created variety of internet sites, designs, brands, and design solutions. A great example of that's this own website, in terms of graphical designing & textual appearances. An equivalent way we've built this page for our business purpose, we could build your own.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
