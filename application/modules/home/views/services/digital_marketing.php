 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>Digital Marketing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>master_assets/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>master_assets/img/service/digital_marketing/seo.svg" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title">We do <span>Search Engine Optimisation:</span></h2>
                        <p>High organic ranking in Google search engine or other major search engine is the key to the business success. Apticks is dedicated to assist clients to extend organic traffic. We help the client establish a brand in a significant way, so you get noticed ahead of the entire world. We are the most effective SEO services company, providing the very best significant quality of SEO and SEM services.</p>
                        <p>Our main goal is to produce cost effective and top quality services that are easily deployed in small or established organizations. Our approach to our work in a unique way,  Quite different from others. We follow the latest and best SEO practices to assist you reach the highest ranks within the search engine pages.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<!-- IOS area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">We do<span>Social Media Marketing</span></h2>
                        <p>Our social media marketing services are an integral part of our SEO services, where, we've a team of experts, having vast experience and sound knowledge of the newest trends in social media marketing. the reasons why we've established ourselves as a reliable social media optimization company, are:</p>
                        <ul>
                        	<li>We assist you to succeed in your marketing goals through a cost effective way. We assist you to communicate together with your customers in an efficient and seamless way, so on get an insight on their behavior, that can assist you to know them in a better way.</li>
                        	<li>Our aim, as a social media marketing agency, is to always improve upon our social media services, in order that they're more result oriented. We also provide social media marketing services for small businesses, helping them to achieve a strong foothold of the social media platforms, through getting increasing numbers of followers, as an example, following on Twitter, and buying false Facebook likes.</li>
                        	<li>Our social media services are designed to extend your brand identity in an emphatic way, on all the social media platforms, like, having real Instagram followings. Our social media marketing services for small businesses are aimed toward helping startup companies to kick off their marketing campaign in a very significant way.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/digital_marketing/smm.svg" alt="video">
                </div>
            </div>
            
        </div>
    </div>

<!-- IOS area End -->
<br>
<br>
<!-- Hybrid area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/digital_marketing/email.svg" alt="video">
                </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">We do<span>Email marketing: </span></h2>
                        <p>Email marketing is now being considered because the best carrier for online advertisement. With the right kind of solutions in email marketing, you'll not only be able to retain the existing customers, but also build up a new database of consumers. Email marketing allows you to use the attractive and professional email marketing communications in order that you stay connected with the clients. At Apticks, we do precisely the same, delivering the simplest email marketing solutions, to assist you to boost up your customer communication. Our bulk email service allows you to send hundreds and even thousands of emails on a daily basis. The good accuracy level is ensured with us, which makes us a number one bulk email service provider.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<!-- Hybrid area End -->
