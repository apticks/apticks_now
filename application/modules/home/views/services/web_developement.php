 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>Web Design & Development</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->


<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>master_assets/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>master_assets/img/service/web_development/what_is_web.svg" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title">What is the <span>Web application?</span></h2>
                        <p>Web applications are programs that are accessed using a browser . they're installed in a server and accessed from a client computer. The server are often on the web or it can be locally within the premises on a local Area Network.</p>
                        <p>The advantages of web application are, they will be accessed from anywhere and that they are available 24 X 7. Any update done on the server program will enable all users to check the same immediately.</p>
                        <p>We not only do web design with some Forms and other simple features like most of the web design Companies. We also develop full fledged web applications. This makes us one among the well-liked vendors, since we will handle both web design and application development. By this you'll save lot of your effort of coordinating with two agencies.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- vission area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/web_development/deliver.svg" alt="video">
                </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">What we <span>Deliver?</span></h2>
                        <p>We are experienced professionals web design development services from many years, Who can understand and determine what the company or its customersí needs within the present IT industry. We make your web design tidy and can work right from the scratch or else can provides it a far better and fascinating look for affordable prices.</p>
                        <p>We make the web design in sync with the logo and the services and the content that's present. once we provide the outstanding web designs, there'll be complete scope to get the leads and also to induce the online site visitors for every company. this is often the straightforward and also effective manner to get enhanced ROI in less time. you simply spend on your own web design and web development and check the difference within the business which you'll be able to make.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

<!-- vission area End -->
<br>
<br>
<!-- Designs area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Most Fabulous <span>Designs</span></h2>
                        <p>Get the stunning web designs then enhance your brand identity. All the customizations which are made here are going to be useful to assist your business sales and services. with our assistance of experience team with great expertise needless to say there'll be tremendous change for good. In every responsive web design that we make, there'll be lot of features like grid based layout and therefore the images which are apt to the service and content.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/web_development/design.svg" alt="video">
                </div>
            </div>
            
        </div>
    </div>

<!-- Designs area End -->
<br>
<br>
<!-- Structure area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/web_development/structure.svg" alt="video">
                </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about"><span>Structure</span> your Website in Splendid Way</h2>
                        <p>Whatever could be ideas which the clients have, our staff are going to be ready to research on an equivalent and produce them to reality. As per the concept of the business we start with the sketch taking all the ideas, possibilities and specifications of the client. We move then understand the customersí requirements also. good deal of attention is given to coding, technology, content, images and provides the web site an inventive and trend which will vary from all the clients competitors.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<!-- Structure area End -->
<br>
<br>
<!-- Unleash area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about"><span>Unleash</span> the Potential of Business</h2>
                        <p>We have the meticulous staff who are experienced in web design development services and that they have the strategic approach to figure on anything. For this they begin with the competitor analysis and also there'll be perfect planning as per the requirements that are given by the clients. we select the most effective web design and web development based on the marketing insights and with this we assure success to the clients without fail. Also, we confirm to figure on the logo design and also provide at the top the foremost responsive web designs. With this, there'll be wide selection of visitorís flow to the web site from various resources. The UI/ UX aspects also are give utmost importance.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/service/web_development/unleash.svg" alt="video">
                </div>
            </div>
        </div>
    </div>

<!-- Unleash area End -->