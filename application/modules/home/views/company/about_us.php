 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->


<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>master_assets/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>master_assets/img/about-us.svg" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title">We is the <span>APTICKS</span></h2>
                        <p>Apticks is at the forefront as the most innovative web development company. Our cost efficient and structured teamwork justifies our position. Apticks was setup to leverage the "Proven India Advantage" in delivering low cost IT software services with a guarantee of Quality with Speed. We have evolved into a large team of professionals with diverse skills and experience.</p>
                        <p>Apticks believes that the perfect Leadership alone will bring the revolutions, not simply the Technology. each skilled at Apticks has unique leadership qualities, Who area unit determined, fearless, and skilful. Their spirit and values area unit contagious that inspire the complete team to deliver their best to our reputable clinets.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- vission area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/goal2.gif" alt="video">
                </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Our<span>Vission?</span></h2>
                        <p>To provide complete IT innovative solutions to form our customers and industry successful through determination, Creativity, Passion, Productivity, Value for money, Ensuring our products of outstanding quality, Instill pride of ownership by innovative, And sustainable technical solutions.</p>
                        <p>We believe every software package are going to be a part of a platform ecosystem, and software expertise will become key to success within the digital world.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

<!-- vission area End -->
<br>
<br>
<!-- mission area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Our<span>Mission?</span></h2>
                        <p>The mission of the Apticks  is to explore, understand and explain the origin and nature of service in the evolution of intelligence and enjoying innovation and applying technology for improving the lives of people as well as, Enchancing the image of worked in high technology.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>master_assets/img/icons/check.svg" alt="check">
                                <span>Concept Design</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>master_assets/img/icons/check.svg" alt="check">
                                <span>Implementation</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>master_assets/img/icons/check.svg" alt="check">
                                <span>Development</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>master_assets/img/icons/check.svg" alt="check">
                                <span>Testing work</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                    <img src="<?php echo base_url();?>master_assets/img/mission.gif" alt="video">
                </div>
            </div>
            
        </div>
    </div>

<!-- mission area End -->
