
<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(master_assets/img/page-title-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Contact</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- contact form start -->
<div class="contact-form-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center w-100">
                    <h2 class="title">Send you <span>inquary</span></h2>
                    <p>We Offer your business with the best assistance for growth.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5">
                <img src="<?php echo base_url()?>master_assets/img/others/21.png" alt="blog">
            </div>
            <div class="col-lg-7 offset-xl-1"><div id='contact_data'></div>
                <form class="riyaqas-form-wrap mt-5 mt-lg-0 " enctype="multipart/form-data" method="post" name="contact_form" id="contact_form" >

                    <div class="row custom-gutters-16" id="success" >
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" class="single-input" id="name" name="name">
                                <label>Name</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" class="single-input" id="email" name="email">
                                <label>E-mail</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" class="single-input" id="subject" name="subject">
                                <label>Subject</label>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" class="single-input" id="mobile" name="mobile">
                                <label>Mobile</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="single-input-wrap">
                                <textarea class="single-input textarea" cols="20" id="message" name="message"></textarea>
                                <label class="single-input-label">Message</label>
                            </div>
                        </div>
                        <div class="col-12">
                             <!--  <input type="submit" class="btn btn-primary"  value="Send" id="butsave"> <span class="output_message"></span> -->
                              <input type="submit" class="btn btn-primary" vallue="submit"id="user_submit">
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
     
        </div>
<!-- map area start -->
<div class="map-area pd-top-120">
    <div class="container">
        <div class="map-area-wrap">
            <div class="row no-gutters">
                <div class="col-lg-8">
                    <div id="map"></div>
                </div>  
                <div class="col-lg-4 desktop-center-item">
                    <div>
                        <div class="contact-info">
                            <h4 class="title">Contact info:</h4>
                            <p class="sub-title">------------</p>
                            <p>Address: Madhapur <br> Hyderabad</p>
                            <p><span>Mobile:</span> +91 7013907291</p>
                            <p><span>E-mail:</span> info@apticks.com, apticksit@gmail.com</p>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- map area End -->
